class ViewMode {
    constructor(prefix) {

        // Main properties
        this.prefix   = prefix;
        this.dom      = getID(prefix + '-mode');
        this.links    = getID(prefix + '-links');
        this.content  = getID(prefix + '-content');
        this.screen   = getID(prefix + '-screen');
        this.btnFull  = getID(prefix + '-fullscreen');
        this.buttons  = [];

        // Colors
        this.colors = ['mediumslateblue', 'mediumspringgreen', 'mediumvioletred', 'mediumpurple', 'mediumturquoise']

        // Element
        this.element = null;

        // Func First Called
        this.listen();

    }

    listen () {
        this.btnFull.addEventListener('click', () => {
            this.screen.requestFullscreen()
        });
    }

    set (el, dir = false) {

        this.element = el;

        let classes = [];
        if (dir) classes.push('enter-' + dir);

        let display = createEl('div', ['view-display'].concat(classes), {}, this.element.content);
        display.style.backgroundColor = this.colors[this.element.id % this.colors.length];
        this.screen.append(display);


        this.buttons = [];
        this.links.innerHTML = '';
        this.element.getRelations().forEach(rel => {
            let arrow;
            switch (rel.targetSection.toString()) {
                case "1":
                    arrow = 'down';
                    break;
                case "2":
                    arrow = 'right';
                    break;
                case "3":
                    arrow = 'up';
                    break;
                case "4":
                    arrow = 'left';
                    break;

            }
            let button = createEl('button', [], {}, `${ this.element.sections[rel.ownSection] ? this.element.sections[rel.ownSection] : arrow + ' : To ' + rel.targetElement.content }`)
            this.links.append(button);
            this.buttons[rel.ownSection] = button;

            button.addEventListener('click', () => {
                let dir = '';
                switch (rel.ownSection.toString()) {
                    case "1":
                        dir = 'down';
                        break;
                    case "2":
                        dir = 'left';
                        break;
                    case "3":
                        dir = 'up';
                        break;
                    case "4":
                        dir = 'right';
                        break;
                }

                display.classList.remove('enter-down', 'enter-up', 'enter-left', 'enter-right');
                display.classList.add('leave-' + dir);
                this.set(rel.targetElement, dir);
                setTimeout(() => {
                    display.remove();
                }, 500);

            })



        });

    }


}