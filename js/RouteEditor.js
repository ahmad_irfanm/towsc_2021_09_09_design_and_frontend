class RouteEditor {
    constructor(app) {

        // Define DOM
        this.dom = getID(app);
        this.dev = false;

        // Define Objects
        this.elements = [];
        this.links    = [];

        // Event Listener
        this.event = {
            shift : false,
        };

        // Func Frist Called
        this.listen();

    }

    generate (elements = [], links = []) {
        if (elements.length == 0 && links.length == 0) {

            let x  = this.dom.scrollWidth / 2,
                y  = this.dom.scrollHeight / 2,
                id = 1;

            // Create initial element as root
            this.createElement(x, y, 1);

        } else {
            // Restore elements
            elements.forEach(({ x, y, id, content, sections }) => {
                let el = this.createElement(x, y, id);
                el.content  = content;
                el.sections = sections;
            });

            links.forEach(({ id, el1, el2, sec1, sec2 }) => {
                el1 = this.elements.find(el => el.id == el1);
                el2 = this.elements.find(el => el.id == el2);

                this.createLink(el1, el2, sec1, sec2, id);
            });
        }
    }

    listen () {

        // Event Mouse Down
        this.dom.addEventListener('mousedown', (e) => {
            this.elements.filter(el => el.event.mousedown).forEach(el => {
                let rect = this.dom.getBoundingClientRect();
                if (this.event.shift) {

                    alert('tst');
                    el.clone = el.make('foreignObject', {
                        width: el.r,
                        height: el.r,
                        x: e.clientX - rect.left,
                        y: e.clientY - rect.top,
                        transform: `translate(-${ el.r / 2 }, -${ el.r / 2 })`,
                        overflow: 'visible',
                        'data-section': e.target.getAttribute('data-section'),
                    });
                    this.dom.prepend(el.clone);

                    if (!e.target.classList.contains('section')) {
                        el.clone.append(e.target.parentElement.cloneNode(true))
                    } else {
                        el.clone.append(e.target.cloneNode(true))
                    }
                }
            });
        });

        // Event Mouse Move
        this.dom.addEventListener('mousemove', (e) => {

            let rect = this.dom.getBoundingClientRect(),
                x    = e.clientX - rect.left,
                y    = e.clientY - rect.top;

            this.elements.filter(el => el.event.mousemove).forEach(el => {
                el.x = x;
                el.y = y;
            });

        });

        // Event Mouse Up
        this.dom.addEventListener('mouseup', (e) => {
            this.elements.filter(el => el.event.mousedown && !el.event.mousemove).forEach(el => {
                let section = e.target.getAttribute('data-section');
                if (!el.getRelations().find(rel => rel.ownSection == section) && section) {
                    this.newElement(el, section);
                }
            });
            this.elements.forEach(el => {
                el.event = {
                    mousedown : false,
                    mousemove : false,
                }
            });
        });

    }

    update () {
        requestAnimationFrame(this.update.bind(this));

        // Update here
        if (!this.dev) {
            let elements = this.elements.map(({ id, sections, content, x, y }) => {
                return { id, sections, content, x, y };
            });
            localStorage.setItem('elements', JSON.stringify(elements));

            let links = this.links.map(({ id, el1, el2, sec1, sec2 }) => {
                return { id, el1: el1.id, el2: el2.id, sec1, sec2 };
            });
            localStorage.setItem('links', JSON.stringify(links));
        }
    }

    createElement (x, y, id) {

        // Create Element
        let element = new Element(x, y, id);

        // Save The Element into Objects
        this.elements.push(element);

        // Return Element
        return element;
    }

    createLink (el1, el2, sec1, sec2, id) {

        // Create Link
        let link = new Link(el1, el2, sec1, sec2, id);

        // Save The Link into Objects
        this.links.push(link);

        // Return Link
        return link;

    }

    newElement (el1, sec1) {

        let x = el1.x,
            y = el1.y,
            el2, sec2;

        switch (sec1.toString()) {
            case "1":
                y -= el1.r * 3;
                sec2 = 3;
                break;
            case "2":
                x += el1.r * 3;
                sec2 = 4;
                break;
            case "3":
                y += el1.r * 3;
                sec2 = 1;
                break;
            case "4":
                x -= el1.r * 3;
                sec2 = 2;
                break;
        }

        el2 = this.createElement(x, y, this.elements.length + 1);
        this.createLink(el1, el2, sec1, sec2, this.links.length + 1);

    }


}