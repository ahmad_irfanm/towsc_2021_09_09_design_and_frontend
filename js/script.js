window.onload = init();

// The first called
function init () {
    // Define the Model
    routeEditor = new RouteEditor( 'app' );
    viewMode    = new ViewMode( 'view' );
    popup       = new Popup( 'popup' );

    // Define elements & links
    let elements = [], links = [];

    // Checking elements & links on storage
    if (localStorage.getItem('elements')) {
        elements = JSON.parse(localStorage.getItem('elements'));
    }
    if (localStorage.getItem('links')) {
        links = JSON.parse(localStorage.getItem('links'));
    }

    // Generate Route Editor
    routeEditor.generate(elements, links);
    routeEditor.update();

    // Global Listen
    listen();
}

// Short func of get element by ID
function getID (id) {
    return document.getElementById(id);
}

// Short func of create general element
function createEl (tag, classes = [], attrs = { }, html = false) {
    // Create tag with Namespace
    tag = document.createElement(tag);

    // Set classes
    classes.forEach(_class => tag.classList.add(_class));

    // Set the attributes
    Object.keys(attrs).forEach(key => tag.setAttribute(key, attrs[key]));

    // Set HTML Content
    if (html) tag.innerHTML = html;

    // Return tag
    return tag;
}

// Global Listener
function listen () {
    // Event Keydown
    window.addEventListener('keydown', (e) => {
        let key = e.keyCode, button;

        switch (key) {
            case 8:
                routeEditor.links.filter(link => link.event.selected).forEach(link => link.destroy());
                break;
            case 16:
                this.event.shift = true;
                break;
            case 49:
                button = viewMode.buttons.find((button, i) => i === 1);
                if (button) button.click();
                break;
            case 50:
                button = viewMode.buttons.find((button, i) => i === 2);
                if (button) button.click();
                break;
            case 51:
                button = viewMode.buttons.find((button, i) => i === 3);
                if (button) button.click();
                break;
            case 52:
                button = viewMode.buttons.find((button, i) => i === 4);
                if (button) button.click();
                break;
        }
    });

    // Event Keyup
    window.addEventListener('keyup', (e) => {
        let key = e.keyCode;

        switch (key){
            case 16:
                routeEditor.event.shift = false;
                routeEditor.elements.forEach(el => {
                    if (el.clone) el.clone.remove();
                    el.clone = null;
                } );
                break;
        }

    });

    // Data Mode On Click
    document.querySelectorAll('[data-mode]').forEach(el => {
        el.addEventListener('click', (e) => {
            e.preventDefault();

            let mode = el.getAttribute('data-mode');
            let main = getID('main');

            main.classList.remove('on-view', 'on-editor');
            main.classList.add('on-' + mode);

            if (mode === 'view') {
                viewMode.set(routeEditor.elements[0]);
            }
        });
    });
}