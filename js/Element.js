class Element extends Svg {
    constructor(x, y, id) {
        super();

        // Main Properties
        this.x  = x;
        this.y  = y;
        this.id = id;
        this.r  = 40;

        // Define DOM
        this.dom   = null;
        this.clone = null;

        // Event Listener
        this.event = {
            mousedown : false,
            mousemove : false,
        };

        // Define Content
        this.content  = 'Slide ' + this.id;
        this.sections = {1: '', 2: '', 3: '', 4: ''};

        // Define Button Actions
        this.btn = {
            edit    : null,
            destroy : null,
        }

        // Func First Called
        this.create();
        this.listen();
        this.update();

    }

    create () {
        // Create Element DOM
        this.dom = this.make('foreignObject', {
            x: this.x,
            y: this.y,
            width: this.r * 2,
            height: this.r * 2,
            class : 'element',
            transform: `translate(-${this.r}, -${this.r})`,
        });
        routeEditor.dom.append(this.dom);

        // Create Circle DOM
        let circle = createEl('div', [ 'circle' ]);
        this.dom.append(circle);

        // Create Sections DOM
        [1, 2, 4, 3].forEach(section => {
            section = createEl('div', [ 'section', `section-${section}` ], { 'data-section' : section}, `<span data-section="${section}">${section}</span>`);
            circle.append(section);
        });

        // Create Button Edit
        this.btn.edit = createEl('button', [ 'btn-action', `btn-edit` ], { }, 'E');
        this.btn.edit.addEventListener('click', () => this.edit());
        this.dom.prepend(this.btn.edit);

        // Create Button Delete
        this.btn.destroy = createEl('button', [ 'btn-action', `btn-delete` ], { }, 'X');
        this.btn.destroy.addEventListener('click', () => this.destroy());
        this.dom.prepend(this.btn.destroy);
    }

    listen () {

        // Event Mouse Down
        this.dom.addEventListener('mousedown', () => {
            this.event.mousedown = true;
        });

        // Event Mouse Move
        this.dom.addEventListener('mousemove', () => {
            if (this.event.mousedown) {
                this.event.mousemove = true;
            }
        });

    }

    update () {

        requestAnimationFrame(this.update.bind(this));

        // Update Position
        this.dom.setAttribute('x', this.x);
        this.dom.setAttribute('y', this.y);

        // Moving Condition
        if (this.event.mousemove) this.dom.classList.add('moving');
        else this.dom.classList.remove('moving');

    }

    getRelations () {
        let relations = routeEditor.links.filter(link => {
            return link.el1.id == this.id || link.el2.id == this.id;
        }).map(link => {
            return {
                link,
                ownElement    : link.el1.id == this.id ? link.el1 : link.el2,
                targetElement : link.el1.id == this.id ? link.el2 : link.el1,
                ownSection    : link.el1.id == this.id ? link.sec1 : link.sec2,
                targetSection : link.el1.id == this.id ? link.sec2 : link.sec1,
            }
        });

        return relations;
    }

    edit () {
        popup.set(this);
    }

    destroy () {
        if (routeEditor.elements.length == 1) {
            alert('Cannot delete the root element !');
            return;
        }
        // Remove Links are Related
        this.getRelations().forEach(rel => rel.link.destroy());

        // Remove Element
        this.dom.remove();
        routeEditor.elements.splice(routeEditor.elements.findIndex(el => el.id == el.id), 1);

    }

}