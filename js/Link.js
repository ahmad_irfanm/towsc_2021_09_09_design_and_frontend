class Link extends Svg {
    constructor(el1, el2, sec1, sec2, id) {
        super();

        // Main Properties
        this.el1  = el1;
        this.el2  = el2;
        this.sec1 = sec1;
        this.sec2 = sec2;
        this.id   = id;

        // Define DOM
        this.dom = null;

        // Event Listener
        this.event = {
            selected : false,
        };

        // Func First Called
        this.create();
        this.listen();
        this.update();

    }

    create () {

        // Get Position
        let pos = this.getPosition();

        // Create Line DOM
        this.dom = this.make('line', {
            x1: pos.el1.x,
            y1: pos.el2.y,
            x2: pos.el2.x,
            y2: pos.el2.y,
            class : 'line',
            stroke: '#111',
            'stroke-width': '3px',
        });
        routeEditor.dom.prepend(this.dom);

        this.el1.dom.querySelector(`.section-${this.sec1}`).classList.add('active');
        this.el2.dom.querySelector(`.section-${this.sec2}`).classList.add('active');

    }

    listen () {

        // Event On Click
        this.dom.addEventListener('click', () => {
            routeEditor.links.forEach(link => link.event.selected = false);
            this.event.selected = true;
        });

    }

    update () {

        requestAnimationFrame(this.update.bind(this));

        // Get Position
        let pos = this.getPosition();

        // Update Position
        this.dom.setAttribute('x1', pos.el1.x);
        this.dom.setAttribute('y1', pos.el1.y);
        this.dom.setAttribute('x2', pos.el2.x);
        this.dom.setAttribute('y2', pos.el2.y);

        // Selected Condition
        if (this.event.selected) this.dom.style.stroke = '#FF0000';
        else this.dom.style.stroke = '#111';

    }

    getPosition () {
        let el1 = {
            x: this.el1.x,
            y: this.el1.y,
        }, el2 = {
            x: this.el2.x,
            y: this.el2.y,
        };

        switch (this.sec1.toString()) {
            case "1":
                el1.y -= this.el1.r;
                break;
            case "2":
                el1.x += this.el1.r;
                break;
            case "3":
                el1.y += this.el1.r;
                break;
            case "4":
                el1.x -= this.el1.r;
                break;
        }

        switch (this.sec2.toString()) {
            case "1":
                el2.y -= this.el2.r;
                break;
            case "2":
                el2.x += this.el2.r;
                break;
            case "3":
                el2.y += this.el2.r;
                break;
            case "4":
                el2.x -= this.el2.r;
                break;
        }

        return {el1, el2};
    }

    destroy () {
        this.el1.dom.querySelector(`.section-${this.sec1}`).classList.remove('active');
        this.el2.dom.querySelector(`.section-${this.sec2}`).classList.remove('active');

        this.dom.remove();
        routeEditor.links.splice(routeEditor.links.findIndex(link => link.id == this.id), 1);
    }

}