class Popup {
    constructor(prefix) {

        // Main properties
        this.prefix   = prefix;
        this.dom      = getID(prefix);
        this.links    = getID(prefix + '-links');
        this.content  = getID(prefix + '-content');
        this.saveBtn  = getID(prefix + '-save');
        this.closeBtn = getID(prefix + '-close');
        this.inputs   = [];

        // Element
        this.element = null;

        // Func First Called
        this.listen();

    }

    listen () {
        this.closeBtn.addEventListener('click', (e) => {
            e.preventDefault();
            this.close();
        });

        this.saveBtn.addEventListener('click', (e) => {
            e.preventDefault();
            this.save();
            this.close();
        });
    }

    set (el) {

        this.element = el;

        this.content.value = this.element.content;


        this.inputs = [];
        this.links.innerHTML = '';
        this.element.getRelations().forEach(rel => {
           let input = createEl('input', [], { placeholder: `Section ${rel.ownSection}` });
           input.value = this.element.sections[rel.ownSection] ? this.element.sections[rel.ownSection] : '';
           this.links.append(input);
           this.inputs[rel.ownSection] = input;
        });

        this.open();

    }

    open () {
        this.dom.classList.add('active');
    }

    close () {
        this.dom.classList.remove('active');
    }

    save () {
        this.element.content = this.content.value;
        this.inputs.forEach((input, section) => {
           this.element.sections[section] = input.value;
        });
    }

}