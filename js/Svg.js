class Svg {
    constructor() {
        // Define the Namespace
        this.namespace = 'http://www.w3.org/2000/svg';
    }
    make (tag, attrs) {
        // Create tag with Namespace
        tag = document.createElementNS(this.namespace, tag);

        // Set the attributes
        Object.keys(attrs).forEach(key => tag.setAttribute(key, attrs[key]));

        // Return tag
        return tag;
    }
}